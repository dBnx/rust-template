use anyhow::Result;

fn main() -> Result<()> {
    dummy::setup_tracing_subscriber()?;

    let _ = dummy::foo(2);

    tracing::debug!("Hello, world ...");
    tracing::info!("Hello, world.");
    tracing::warn!("Hello, world?");
    tracing::error!("Hello, world!");

    Ok(())
}
