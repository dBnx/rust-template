use anyhow::{bail, Result};

pub fn setup_tracing_subscriber() -> Result<()> {
    let subscriber = tracing_subscriber::fmt()
        // Use a more compact, abbreviated log format
        .compact()
        // Display source code file paths
        .with_file(true)
        // Display source code line numbers
        .with_line_number(true)
        // Display the thread ID an event was recorded on
        .with_thread_ids(true)
        // Don't display the event's target (module path)
        .with_target(false)
        // Build the subscriber
        .finish();
    tracing::subscriber::set_global_default(subscriber)?;
    Ok(())
}

#[tracing::instrument]
pub fn foo(x: u8) -> Result<u8> {
    if x == 0 {
        bail!("Oh no, you passed 0");
    }
    Ok(x)
}

#[cfg(test)]
mod tests {
    use super::*;
    #[allow(unused_imports)]
    use pretty_assertions::{assert_eq, assert_ne, assert_str_eq};
    #[allow(clippy::wildcard_imports)]
    use rstest::*;

    #[fixture]
    fn setup() {
        #[allow(clippy::unwrap_used)]
        color_eyre::install().unwrap();
    }

    #[test]
    fn foo_pass() -> Result<()> {
        assert_eq!(foo(1)?, 1);
        Ok(())
    }

    #[test]
    #[should_panic]
    fn foo_fail() {
        assert_eq!(foo(1).unwrap(), 2);
    }
}
