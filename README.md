# Template for Rust Projects

This project implements various tooling to automate and simplify the
development process as much as possible.

## [Pre-Commit](https://pre-commit.com)

### Install the hooks

This only has to be done once. Simply run the following command after cloning
the repository:

```sh
pre-commit install
```

## Pipeline

The project contains a pipeline configuration that runs tests against rust's
stable and nightly release. After that the documentation and binary is generated
for the `dev` and `main` branches. The `main` branch also publishes the artifacts
to GitLab pages.

## Tests

## Customizing the project

You'd have to change the name of the binary/library in:

- `Cargo.toml`:  At the top.
- `.gitlab-ci.yml`: The binary path.
- `main.rs`: Dummy project files, if kept, contain the library name.
